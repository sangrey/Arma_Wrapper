This repository contains code that allows one to efficiently wrap C++ functions that use Armadillo functions and 
call them from Python. 


It is composed of two parts. 
The first is in function_wrapper.h. 
This provides a type-safe template-based method of wrapping functions and using two templated functors. 
Consider the following. 

To try to make the idea a bit more concrete, we have the following. The idea here is to you have a function 
f that takes Args..., and returns ReturnVals... You want a function g that takes h1(Args).... and returns 
h2(ReturnVals)... In other words, g = h2(f(h1(args)...)..), where we are abusing the variadic template syntax. 


The other header file provides various functions for trasferring Armadillo matrices and cubes into Numpy arrays.
It also includes several variadic assertion statements that allow you to assert several releated statuements
all at the same time.


The arma_wrapper.cpp file exposes a few wrapper classes for Armadillo and exposes them to Python.  
It builds a Python package that can be downloaded from https://anaconda.org/sangrey/arma_wrapper using conda.
