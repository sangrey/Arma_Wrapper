import numpy as np
import pytest
from hypothesis import given,settings,strategies as st 
import re 

# I want the same random values created every time.
np.random.seed(5345248)


# noinspection PyShadowingNames
def test_arma_wrapper_import():
    """ This tests ensures that I can import the file """ 
    import arma_wrapper

@given(st.lists(st.floats(allow_infinity=False, allow_nan=False, max_value=1000, min_value=-1000)))
def test_sum(thing):
    """ Ensures that the sum is giving the correct value. """ 
    import arma_wrapper as aw 
   
    err_msg =  "The summation function is not giving the correct answer." 
    assert np.isclose(aw.sum(thing), np.sum(thing), equal_nan=True), err_msg 


@given(st.integers(min_value=0, max_value=20).flatmap(lambda n: st.lists(st.lists(
            st.integers(min_value=-2**31+2, max_value=2**31-2), min_size=n, max_size=n), 
       min_size=0)).map(np.asarray))
def test_intmat_serialization(int_thing):
    """ When you convert the ndarray into a matrix and back you should get the same result back."""

    import arma_wrapper as aw
  
    if int_thing.size == 0: 
        int_thing = np.atleast_2d(int_thing)

    int_mat_err_msg = "We cannot convert integer matrices into aw.intmat and then back again. "
    assert np.array_equal(aw.intmat_identity(int_thing), int_thing), \
                 int_mat_err_msg 


@given(st.integers(min_value=0, max_value=20).flatmap(lambda n: st.lists(st.lists(
            st.floats(), min_size=n, max_size=n), min_size=0)).map(np.asarray))
def test_mat_serialization(arr):
    """ When you convert the ndarray into a matrix and back you should get the same result back."""

    import arma_wrapper as aw
    np.seterr(invalid='ignore')
  
    arr = arr.astype(np.float64)
    if arr.size == 0:
        arr = np.atleast_2d(arr)    
 
    double_mat_err_msg = "We cannot convert double matrices into aw.matrices and then back again." 
    assert np.allclose(aw.mat_identity(arr), arr, equal_nan=True), double_mat_err_msg



@given(st.tuples(st.integers(min_value=0, max_value=10), st.integers(min_value=0, max_value=10)).flatmap(
            lambda n: st.lists(st.lists(st.lists(
            st.floats(), min_size=n[0], max_size=n[0]), min_size=n[1], max_size=n[1]), 
                min_size=0)).map(np.asarray))
def test_cube_serialization(arr):                                                                                               
    """ When you convert the ndarray into a cube and back you should get the same result back."""                     

    import arma_wrapper as aw
    np.seterr(invalid='ignore')
        
    if arr.size == 0:
        arr = np.atleast_3d(arr.astype(np.float64))
     
    double_cube_err_msg = "We cannot convert double cubes into aw.cubes and then back again." 
    assert np.allclose(aw.cube_identity(arr), arr, equal_nan=True), double_cube_err_msg


@given(st.lists(st.floats(), min_size=0).map(lambda x: np.reshape(x, (len(x), 1,1))))
def test_cube_dimension_reordering(arr):
    """ Ensures that the dimensions being passed in have the same dimensions when they are returned """

    from arma_wrapper import cube_identity

    if arr.size == 0:
        arr = np.atleast_3d(arr)

    assert np.allclose(cube_identity(arr), arr, equal_nan=True), " The dimensions are reshuffled. "

    assert cube_identity(arr).shape == arr.shape, "THe shapes are not the same. "

@settings(deadline=None)
@given(st.lists(st.floats(), min_size=1).map(np.asarray)) 
def test_sparse_matrix_serialization(arr):


    from scipy import sparse
    from arma_wrapper import identity, duplicate    
    
    mat = sparse.csc_matrix(arr)

    assert np.allclose(mat.toarray(), identity(mat).toarray(), equal_nan=True), \
        'We cannot pass sparse matrices to and from C++ correctly.'



@given(st.lists(st.floats()))
def test_vector_serialization(arr):


    from arma_wrapper import col_identity, row_identity
    np.seterr(invalid='ignore')

    assert np.allclose(arr, col_identity(arr), equal_nan=True),\
            'We cannot pass column vectors to and from armadillo correctly.'


    assert np.allclose(arr, row_identity(arr), equal_nan=True), \
            'We cannot pass row vectors to and from armadillo correcty.'


@given(st.lists(st.floats()).map(np.asarray))
def test_noncontiguous_matrices(arr):

    from arma_wrapper import mat_identity

    broadcast_array = np.broadcast_to(arr, (2, arr.size))

    contigous_array = np.ascontiguousarray(broadcast_array)

    err_msg = 'We cannot serialize noncontiguous matrices. '
    assert np.allclose(mat_identity(broadcast_array), contigous_array, equal_nan=True), err_msg 


     
@given(st.lists(st.floats(),min_size=0).map(np.asarray))
def test_noncontiguous_cubes1(arr):

    from arma_wrapper import cube_identity

    broadcast_array = np.broadcast_to(arr, (3, arr.size))
    contigous_array = np.ascontiguousarray(broadcast_array)

    err_msg = 'We cannot serialize noncontiguous cubes that arise from broadcasted vectors. '
    assert np.allclose(np.squeeze(cube_identity(broadcast_array)), contigous_array, equal_nan=True), err_msg 


@given(st.integers(min_value=0, max_value=20).flatmap(lambda n: st.lists(st.lists(
            st.floats(), min_size=n, max_size=n), min_size=1)).map(np.asarray))
def test_noncontiguous_cubes2(arr):

    from arma_wrapper import cube_identity

    broadcast_array = np.broadcast_to(arr, (3, *arr.shape))
    contigous_array = np.ascontiguousarray(broadcast_array)

    err_msg = 'We cannot serialize noncontiguous cubes that arise from broadcasted matrices. '
    assert np.allclose(cube_identity(broadcast_array), contigous_array, equal_nan=True), err_msg 


@given(st.lists(st.floats(), min_size=1, max_size=1).map(np.asarray), st.integers(min_value=0, max_value=100))
def test_noncontiguous_vectors(arr, dim):

    from arma_wrapper import col_identity

    broadcast_array = np.broadcast_to(arr, dim)

    contigous_array = np.ascontiguousarray(broadcast_array)

    err_msg = 'We cannot serialize noncontiguous vectors that arise from broadcasted flats.'

    assert np.allclose(col_identity(broadcast_array), contigous_array, equal_nan=True), err_msg



@given(st.lists(st.floats(), min_size=1, max_size=1).map(np.asarray), st.integers(min_value=0, max_value=100))
def test_noncontiguous_rows(arr, dim):

    from arma_wrapper import row_identity

    broadcast_array = np.broadcast_to(arr, dim)

    contigous_array = np.ascontiguousarray(broadcast_array)

    err_msg = 'We cannot serialize noncontiguous row vectors that arise from broadcasted flats.'

    assert np.allclose(row_identity(broadcast_array), contigous_array, equal_nan=True), err_msg
